﻿class MainClass
{
    public static void Main(string[] args)
    {
        bool exitIndicator = false;


        Shop myShop = new Shop("Pavel's shop", "Kozlova 8");
        myShop.items.Add(new Vegetiran("Cucumber", 2, 5));
        myShop.items.Add(new NotVegeterian("Pork", 12, 2));
        myShop.items.Add(new Vegetiran("Cucumber", 2, 2));
        myShop.items.Add(new Vegetiran("Apple", 0.3, 4));
        myShop.items.Add(new NotVegeterian("Ham", 4, 30));
        myShop.items.Add(new Vegetiran("Salad", 2, 6));
        myShop.items.Add(new UnEatable("Microwave oven", 300));
        myShop.items.Add(new UnEatable("Bycicle", 250));
        myShop.items.Add(new UnEatable("TV", 2000));

        while (exitIndicator == false)
        {
            Console.WriteLine("         \"Store\"\n\n");
            Console.WriteLine(" ________            ________   \n|  Play  |          |  Exit  |\n| Press 1|          | Press 0|\n --------            -------- ");

            string input = Console.ReadLine();
            switch (input)
            {
                case "1":
                    {
                        Console.Clear();
                        bool mainWhileIndicator = false;

                        while (mainWhileIndicator == false)
                        {
                            Console.WriteLine("              let's shooooop");
                            Console.WriteLine("         ________            ________\n        |My items|          | Store  |\n        | Press 1|          | Press 2|\n         --------            --------");
                            Console.WriteLine("                   --------\n                  |  Back  |\n                  | Press 0|\n                   --------");

                            input = Console.ReadLine();
                            switch (input)
                            {
                                case "1":
                                    {
                                        Console.Clear();
                                        while (true)
                                        {
                                            myShop.ShowAllSortedItems();
                                            Console.WriteLine("            what do you want to do?\n\n\n\n");
                                            Console.WriteLine("      _____________            _____________\n     |Add new item |          | Delete items|\n     |   Press  1  |          |   Press  2  |\n      -------------            -------------");
                                            Console.WriteLine();
                                            Console.WriteLine("      _____________            _____________\n     |Show eatable |          |Show uneatab.|\n     |   Press  3  |          |   Press  4  |\n      -------------            -------------");
                                            Console.WriteLine();
                                            Console.WriteLine("                   --------\n                  |  Back  |\n                  | Press 0|\n                   --------");

                                            input = Console.ReadLine();

                                            if (input == "0")
                                            {
                                                Console.Clear();
                                                break;
                                            }

                                            switch (input)
                                            {
                                                case "1":
                                                    {
                                                        myShop.AddItem();
                                                        break;
                                                    }

                                                case "2":
                                                    {
                                                        myShop.DeleteItem();
                                                        break;
                                                    }

                                                case "3":
                                                    {
                                                        //show eatable
                                                        Console.WriteLine("      _____________            _____________            _____________\n     |Show vegeter.|          |  Show other |          |   Show all  |\n     |   Press  1  |          |   Press  2  |          |   Press  3  |\n      -------------            -------------            -------------");
                                                        Console.WriteLine();
                                                        Console.WriteLine("                         --------\n                        |  Back  |\n                        | Press 0|\n                         --------");
                                                        input = Console.ReadLine();
                                                        switch (input)
                                                        {
                                                            case "1":
                                                                {
                                                                    Console.Clear();
                                                                    myShop.ShowVegeterianItems();
                                                                    break;
                                                                }

                                                            case "2":
                                                                {
                                                                    Console.Clear();
                                                                    myShop.ShowNotVegeterianItems();
                                                                    break;
                                                                }

                                                            case "3":
                                                                {
                                                                    Console.Clear();
                                                                    myShop.ShowEatableItems();
                                                                    break;
                                                                }
                                                        }
                                                        break;
                                                    }

                                                case "4":
                                                    {
                                                        Console.Clear();
                                                        myShop.ShowAllUneatableItems();
                                                        break;
                                                    }
                                            }
                                        }
                                        break;
                                    }

                                case "2":
                                    {
                                        Console.Clear();
                                        Console.WriteLine($"Your shop:\n\nName- {myShop.ShopName}\n\nAdress - {myShop.Adress}\n\n\n\n");

                                        break;
                                    }

                                case "0":
                                    {
                                        Console.Clear();
                                        mainWhileIndicator = true;
                                        break;
                                    }
                            }

                        }


                        break;
                    }

                case "0":
                    {
                        Console.Clear();
                        Console.WriteLine("Goodbye)");
                        exitIndicator = true;
                        break;
                    }

            }
        }
        Console.ReadKey();
    }
}





class Shop
{
    public List<Items> items = new List<Items>();
    protected string _shopName;
    protected string _adress;

    public string ShopName
    {
        get { return _shopName; }
        set
        {
            if (value != null)
            {
                _shopName = value;   //ну, проверочка какая-то
            }
        }
    }

    public string Adress
    {
        get { return _adress; }
        set
        {
            if (value != null)
            {
                _adress = value; //опять же проверка
            }
        }
    }


    public Shop(string adress, string shopNname)
    {
        _adress = adress;
        _shopName = shopNname;
    }

    public void AddItem()
    {
        Console.WriteLine("Select type of the item\n\nPress 1 to create an eatable item\n\nPress 2 to create an uneatable item\n\n\n\n");

        string input = Console.ReadLine();
        switch (input)
        {
            case "1":
                {
                    Console.WriteLine("Press 1 to create a vegeterian food\n\nElse press 2\n\n\n\n");
                    input = Console.ReadLine();
                    switch (input)
                    {
                        case "1":
                            {
                                Console.WriteLine("Enter the name of item");
                                string _name = Console.ReadLine();
                                Console.WriteLine("Enter the price of item");
                                double _price = double.Parse(Console.ReadLine());
                                Console.WriteLine("Enter expire time of item");
                                int _expireTime = int.Parse(Console.ReadLine());
                                items.Add(new Vegetiran(_name, _price, _expireTime));
                                Console.WriteLine("New item was added sucsesfully\n\n\n\n");
                                break;
                            }
                        case "2":
                            {
                                Console.WriteLine("Enter the name of item");
                                string _name = Console.ReadLine();
                                Console.WriteLine("Enter the price of item");
                                double _price = double.Parse(Console.ReadLine());
                                Console.WriteLine("Enter expire time of item");
                                int _expireTime = int.Parse(Console.ReadLine());
                                items.Add(new NotVegeterian(_name, _price, _expireTime));
                                Console.WriteLine("New item was added sucsesfully\n\n\n\n");
                                break;

                            }
                        default:
                            {
                                Console.WriteLine("pls enter the normal number\n\n\n\n");
                                break;
                            }
                    }

                    break;
                }

            case "2":
                {
                    Console.WriteLine("Enter the name of item");
                    string _name = Console.ReadLine();
                    Console.WriteLine("Enter the price of item");
                    double _price = double.Parse(Console.ReadLine());
                    items.Add(new UnEatable(_name, _price));
                    Console.WriteLine("New item was added sucsesfully\n\n\n\n");
                    break;
                }
            default:
                {
                    Console.WriteLine("pls enter the normal number\n\n\n\n");
                    break;
                }
        }
    }

    public void DeleteItem()
    {
        Console.WriteLine("Enter the name of item to delete");
        string input = Console.ReadLine();
        items.RemoveAll(anyItem => anyItem.Name == input);
    }

    public void ShowAllSortedItems()
    {
        items.Sort((left, right) => right.Price.CompareTo(left.Price));

        Console.WriteLine("=======================================================");
        Console.WriteLine("There are all items you have:\n\n\n");
        for (int i = 0; i < items.Count; i++)
        {
            Console.WriteLine($"{items[i].Name} - {items[i].Price}$\n");
        }
        Console.WriteLine("=======================================================\n\n\n");
    }


    public void ShowVegeterianItems()
    {
        Console.WriteLine("=======================================================");
        Console.WriteLine("There are all of vegeterian eatable items in your shop");
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] is Vegetiran)
            {
                Console.WriteLine($"{items[i].Name} - {items[i].Price}$\n");
            }
        }
        Console.WriteLine("=======================================================\n\n\n");
    }


    public void ShowNotVegeterianItems()
    {
        Console.WriteLine("=======================================================");
        Console.WriteLine("There are all of not vegetarian eatable items in your shop");
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] is NotVegeterian)
            {
                Console.WriteLine($"{items[i].Name} - {items[i].Price}$\n");
            }
        }
        Console.WriteLine("=======================================================\n\n\n");
    }


    public void ShowEatableItems()
    {
        Console.WriteLine("=======================================================");
        Console.WriteLine("There are all of eatable items in your shop");
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] is Eatable)
            {
                Console.WriteLine($"{items[i].Name} - {items[i].Price}$\n");
            }
        }
        Console.WriteLine("=======================================================\n\n\n");
    }


    public void ShowAllUneatableItems()
    {
        Console.WriteLine("=======================================================");
        Console.WriteLine("There are all of uneatable items in your shop");
        for (int i = 0; i < items.Count; i++)
        {
            if (items[i] is UnEatable)
            {
                Console.WriteLine($"{items[i].Name} - {items[i].Price}$\n");
            }
        }
        Console.WriteLine("=======================================================\n\n\n");
    }

}

abstract class Items
{
    protected string name;
    protected double price;

    public string Name { get { return name; } set { name = value; } }
    public double Price { get { return price; } set { price = value; } }

    public Items(string _name, double _price)
    {
        name = _name;
        price = _price;
    }
}

abstract class Eatable : Items
{
    private int expireTime;

    public int ExpireTime
    {
        get { return expireTime; }
        set
        {
            if (ExpireTime > 1000)
            {
                Console.WriteLine("Enter the normal time, pls");
            }
            expireTime = value;
        }
    }
    public Eatable(string _name, double _price, int _expireTime) : base(_name, _price)
    {
        name = _name;
        price = _price;
        expireTime = _expireTime;

    }
}

class Vegetiran : Eatable, Ivegeterian
{
    public bool ShowVegOrientation()
    {
        bool vegInd = true;
        return vegInd;
        //Console.WriteLine("Vegetirian");
    }

    public Vegetiran(string _name, double _price, int _expireTime) : base(_name, _price, _expireTime)
    {
        name = _name;
        price = _price;
        ExpireTime = _expireTime;
    }
}

class NotVegeterian : Eatable, Ivegeterian
{
    public bool ShowVegOrientation()
    {
        bool vegInd = false;
        return vegInd;
        //Console.WriteLine("Not vegetirian");
    }

    public NotVegeterian(string _name, double _price, int _expireTime) : base(_name, _price, _expireTime)
    {
        name = _name;
        price = _price;
        ExpireTime = _expireTime;
    }
}



class UnEatable : Items
{
    //Something specific;
    public UnEatable(string _name, double _price) : base(_name, _price)
    {
        name = _name;
        price = _price;
    }
}

interface Ivegeterian
{
    bool ShowVegOrientation();
}





























/*
class MainClass
{
    public static void Main(string[] args)
    {
        Shop myShop = new Shop();
    }

    public static void InitialiseShop(Shop shop)
    {

    }

    
}

class Shop
{
    List<Product> products = new List<Product> ();

    public void AddProduct(Product product)
    {
        if (product == null)
        {

        }
    }

    public void ShowEatable()
    {
        for (int i = 0; i < products.Count; i++)
        {
            Console.WriteLine((products[i] as Eatable).ExpireTime);
        }
    }
}

abstract class Product
{
    private int cost;
    public int Cost => cost;

    public Product(int cost)
    {
        this.cost = cost;
    }
}

abstract class Uneatable : Product
{
    public Uneatable(int cost): base(cost)
    {

    }
}

abstract class Eatable: Product
{
    private int expireTime;
    public int ExpireTime => expireTime;

    public Eatable(int cost, int expireTime): base(cost)
    {
        this.expireTime = expireTime;
    }
}



class Apple : Product
{
    public Apple(int cost) : base(cost)
    {

    }
}
*/
